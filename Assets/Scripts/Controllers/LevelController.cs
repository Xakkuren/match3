﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Match3
{
	public class LevelController : MonoBehaviour
	{
		public static LevelController lvlController;
		public string LevelToLoad;

		void Start()
		{
			if (lvlController == null) { lvlController = this.gameObject.GetComponent<LevelController>(); }
		}

		public void LoadLevel()
		{
			//Load the level from LevelToLoad
			if (GameController.gm ? GameController.gm.isPaused : false) { Pause(); }
			SceneManager.LoadScene(LevelToLoad);
		}

		public void Exit()
		{
			Application.Quit();
		}

		public void Pause()
		{
			GameController.gm.Pause();
		}
	}
}
