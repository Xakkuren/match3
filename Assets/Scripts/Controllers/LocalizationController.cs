﻿using UnityEngine;
using UnityEngine.UI;

namespace Match3
{
	public class LocalizationController : MonoBehaviour
	{
		public Text scoreText;
		public Text healthText;
		public Text restartText;
		public Text exitText;
		public Text newGameText;
		public Text resumeText;
		public Text bossText;

		void Start()
		{
			if (scoreText) { scoreText.text = UITexts.scoreText.ToString(); }
			if (healthText) { healthText.text = UITexts.healthText.ToString(); }
			if (restartText) { restartText.text = UITexts.restartText.ToString(); }
			if (exitText) { exitText.text = UITexts.exitText.ToString(); }
			if (newGameText) { newGameText.text = UITexts.newGameText.ToString(); }
			if (resumeText) { resumeText.text = UITexts.resumeText.ToString(); }
			if (bossText) { bossText.text = UITexts.bossText.ToString(); }
		}
	}
}
