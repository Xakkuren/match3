﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Match3
{
	public class GameController : MonoBehaviour
	{
		public static GameController gm;

		public int score = 0;

		public Text infoDisplay;
		public Text scoreDisplay;

		public GameObject ResumeButton;
		public GameObject RestartButton;
		public GameObject ExitButton;

		public bool isGameOver = false;
		public bool isLevelOver = false;

		public bool isPaused { get { return Time.timeScale == 0; } }

		// Use this for initialization
		void Start()
		{
			SetActiveEndButtons(false);

			if (gm == null) { gm = this.gameObject.GetComponent<GameController>(); }
		}

		// Update is called once per frame
		void Update()
		{
			if (isGameOver)
			{
				infoDisplay.enabled = true;
				infoDisplay.text = GameMessages.EndGame.ToString();

				Destory();

				SetActiveEndButtons(true, true);
			}

			if (isLevelOver && !isGameOver)
			{
				infoDisplay.enabled = true;
				infoDisplay.text = GameMessages.WinLevel.ToString();

				Destory();

				SetActiveEndButtons(true, true);
			}

			if (Input.GetKeyDown("escape") && !isGameOver)
			{
				Pause();
			}
		}

		public void AddScore(int scoreAmount)
		{
			score += scoreAmount;
			scoreDisplay.text = score.ToString();
		}

		public void Pause()
		{
			Time.timeScale = Convert.ToInt64(isPaused);
			SetActiveEndButtons(isPaused);
		}

		public void Destory()
		{

		}

		private void SetActiveEndButtons(bool isActive, bool endScreen = false)
		{
			RestartButton.SetActive(isActive);
			ExitButton.SetActive(isActive);
			if (!endScreen) { ResumeButton.SetActive(isActive); }
		}
	}
}
