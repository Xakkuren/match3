﻿namespace Match3
{
	public class TypeSafeEnum
	{
		private readonly string name;

		protected TypeSafeEnum(string name)
		{
			this.name = name;
		}

		public override string ToString()
		{
			return name;
		}
	}
}
