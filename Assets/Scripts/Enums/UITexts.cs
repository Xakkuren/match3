﻿namespace Match3
{
	public sealed class UITexts : TypeSafeEnum
	{
		public static readonly UITexts scoreText = new UITexts("Счет");
		public static readonly UITexts healthText = new UITexts("Здоровье");
		public static readonly UITexts restartText = new UITexts("Переиграть");
		public static readonly UITexts exitText = new UITexts("Выход");
		public static readonly UITexts newGameText = new UITexts("Новая игра");
		public static readonly UITexts resumeText = new UITexts("Продолжить");
		public static readonly UITexts bossText = new UITexts("Босс");

		private UITexts(string name) : base(name) { }
	}
}
