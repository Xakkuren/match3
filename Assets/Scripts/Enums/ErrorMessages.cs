﻿namespace Match3
{
	public sealed class ErrorMessages : TypeSafeEnum
	{
		public static readonly ErrorMessages bulletObjectNotFind = new ErrorMessages("Тест");

		private ErrorMessages(string name) : base(name) { }
	}
}