﻿namespace Match3
{
	public sealed class GameMessages : TypeSafeEnum
	{
		public static readonly GameMessages EndGame = new GameMessages("Потрачено");
		public static readonly GameMessages WinLevel = new GameMessages("Успех");

		private GameMessages(string name) : base(name) { }
	}
}
